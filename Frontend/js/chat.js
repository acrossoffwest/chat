/*
  ChatClass
*/
var ChatClass = function (args) {
  this.audioElement = document.createElement('audio');
  this.audioElement.setAttribute('src', '/audios/sound.wav');
  this.vue = args.vue;
  this.wsData = {
    url: args.connectionUrl
  }; 
  this.events = {
    msgTransfer: 'msgTransfer'
  };

  this.socket = args.io.connect(this.wsData.url);

  var that = this;

  this.socket.on('connect', function () {
    console.log('Connection setup');
  });

  this.socket.on(this.events.msgTransfer, function (data) {
    data.time = that.getCurrentTime('time');
    that.vue.pullMessage.push(data);
    setTimeout(function () {
      $('.chat').scrollTop($('.chat').prop('scrollHeight'));
      if (data.author ==that.vue.newMessage.author) {
        return true;
      }
      that.audioElement.play();
    }, 500);
  });

  this.socket.on('socketsCount', function (count) {
    that.vue.socketsCount = count;
  });

  this.socket.on('disconnect', function () {
    console.log('Server disconnected');
  });
  this.loaded({speed: 500});

}

ChatClass.prototype.getCurrentTime = function (type) {
  
  var date = new Date();
  
  var year = date.getFullYear(); 
  var month = date.getMonth() + 1; 
  var day = date.getDate(); 

  var hours = date.getHours();
  var minutes = date.getMinutes();
  
  switch (type) {
    case 'time':
      return '[' + hours + ':' + minutes + ']';
    case 'date':
      return '[' + year + '-' + month + '-' + day + ']';
    default:
      return '[' + year + '-' + month + '-' + day + ' | ' + hours + ':' + minutes + ']';
  }
};
ChatClass.prototype.loaded = function (args) {
  var speed = args.speed ? args.speed : 250;
  $('title').text($('title').attr('describe'));
  $('.preloader').fadeToggle(speed, function () {
    $('.wrapper').fadeToggle(speed);
  });
};
ChatClass.prototype.validMsg = function (msg) {
  var equal = msg.author === undefined || msg.message === undefined ||
    ((msg.author.replace(/\ /g, '')) === '') ||
    ((msg.message.replace(/\ /g, '')) === '');
  if (!equal) {
    return true;
  } else {
    return false;
  }
};
ChatClass.prototype.emit = function (event, data, cb) {
  if (event === undefined || data === undefined) {
    return false;
  }
  this.socket.emit(event, data);
  cb();
};
ChatClass.prototype.sendMessage = function (msg, cb) {
   if (!this.validMsg(msg)) {
    return cb('Message not send');
   } 
   this.emit(this.events.msgTransfer, msg, function () {
    return cb(null, 'Ok');
   });
};
