/*
  Common scripts
*/
var chatApp = null;
$(function () {
  var viewApp = new Vue({
    el: '#app',
    data: {
      page: {
        title: 'Online chat',
        longTitle: 'Chat created on Backend: NodeJS(LoopBack, Socket.IO), Frontend: VueJS',
        description: 'Просьба не употреблять нецензурные выражения и уважать собеседников в этом чате.'
      },
      newMessage: {
        author: '',
        message: ''
      },
      error: {
        form: {
          invalidData: ''
        }
      },
      pullMessage: [],
      socketsCount: 0
    },
    methods: {
      sendMessage: function (event) {
        var that = this;
        chatApp.sendMessage(this.newMessage, function (err, result) {
          var errMsg = {author: 'System', message: ''};
          if (err) {
            errMsg.message = err;
            return that.pullMessage.push(errMsg);
          }
          return console.log('Result:' + result);
        });
      }
    }
  });

  chatApp = new ChatClass({
    io: io, 
    vue: viewApp, 
    connectionUrl: 'http://chat.dev:4000'
  });
});