module.exports = function (app) {
    app.io.sockets.on('connection', function (socket) {

      console.log('User connected');

      app.io.sockets.emit('socketsCount', Object.keys(app.io.engine.clients).length);

      var msgTransfer = 'msgTransfer';

      socket.on(msgTransfer, function (data) {

        console.log('<MessageTransfer/Get> Author: ' + data.author + '; Message: ' + data.message + ';');

        app.io.sockets.emit(msgTransfer, data);

        console.log('Message transfered. Any body.');

      });

      socket.on('disconnect', function () {
        app.io.sockets.emit('socketsCount', Object.keys(app.io.engine.clients).length);
        console.log('user disconnected');

      });
    });
}